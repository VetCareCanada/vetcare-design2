(function ($) {
    //tabs
    $(".wp-block-vc-blocks-vctab").click(function () {

        const tabPane = $(this).attr("data-vc-target");
        $(".vc-tab-pane").removeClass("active").removeClass("show");
        $(tabPane).addClass("active").addClass("show");

        $(this).siblings().removeClass("active");
        $(this).addClass("active");

    });

    //tabs dropdown
    if ($(document).width() < 992) {
        let tabs = {};
        const tabsEl = $(".vctabs");
        if (tabsEl) {
            const tabsWrapperEl = tabsEl.find(".vctabs-wrapper");
            let activeTab = "";
            let tabsOptions = "";
            if (tabsWrapperEl.length) {
                let pElements = $(tabsWrapperEl).children(".vctab");
                pElements.each(function () {
                    let tabText = $(this).find("p").text();
                    tabs[$(this).attr("data-vc-target")] = tabText;
                    if (activeTab === "" && $(this).hasClass("active")) {
                        activeTab = tabText;
                    }
                });
                console.log(tabs);
                debugger
                if (tabs) {
                    $.each(tabs, function (k, v) {
                        tabsOptions += `<li><a class="dropdown-item" data-vc-target="${k}" href="javascript:;">${v}</a></li>`;
                    });

                    tabsEl.after(`<div class="dropdown mb-5">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuVctabs" data-bs-toggle="dropdown" aria-expanded="false">
${activeTab}
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenuVctabs" id="dropdownMenuVctabsMenu">
   ${tabsOptions}
  </ul>
</div>`);

                    //remove deskto ptabs
                    $(tabsEl).remove();
                }


                //bind click event
                $("#dropdownMenuVctabsMenu a").on("click", function () {
                    let tabId = $(this).attr("data-vc-target");
                    let tabTitle = $(this).text();
                    //hide all tab panes
                    $(".vc-tab-pane").removeClass("active").removeClass("show");
                    //show clicked tab pane
                    $(tabId).addClass("active").addClass("show");
                    //change dropdown text
                    $("#dropdownMenuVctabs").html(tabTitle);
                });
            }
        }
    }


})(jQuery);
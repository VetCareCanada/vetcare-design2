/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/blocks/_wp-block-cover.js":
/*!******************************************!*\
  !*** ./src/js/blocks/_wp-block-cover.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  $(document).ready(function () {
    if ($(".has-zoom-animation").length > 0) {
      $(".has-zoom-animation").addClass("vc-animated");
    }
  });
})(jQuery);

/***/ }),

/***/ "./src/js/blocks/_wp-block-vc-blocks-vctabs.js":
/*!*****************************************************!*\
  !*** ./src/js/blocks/_wp-block-vc-blocks-vctabs.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  //tabs
  $(".wp-block-vc-blocks-vctab").click(function () {
    var tabPane = $(this).attr("data-vc-target");
    $(".vc-tab-pane").removeClass("active").removeClass("show");
    $(tabPane).addClass("active").addClass("show");
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
  }); //tabs dropdown

  if ($(document).width() < 992) {
    var tabs = {};
    var tabsEl = $(".vctabs");

    if (tabsEl) {
      var tabsWrapperEl = tabsEl.find(".vctabs-wrapper");
      var activeTab = "";
      var tabsOptions = "";

      if (tabsWrapperEl.length) {
        var pElements = $(tabsWrapperEl).children(".vctab");
        pElements.each(function () {
          var tabText = $(this).find("p").text();
          tabs[$(this).attr("data-vc-target")] = tabText;

          if (activeTab === "" && $(this).hasClass("active")) {
            activeTab = tabText;
          }
        });
        console.log(tabs);
        debugger;

        if (tabs) {
          $.each(tabs, function (k, v) {
            tabsOptions += "<li><a class=\"dropdown-item\" data-vc-target=\"".concat(k, "\" href=\"javascript:;\">").concat(v, "</a></li>");
          });
          tabsEl.after("<div class=\"dropdown mb-5\">\n  <button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuVctabs\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">\n".concat(activeTab, "\n  </button>\n  <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuVctabs\" id=\"dropdownMenuVctabsMenu\">\n   ").concat(tabsOptions, "\n  </ul>\n</div>")); //remove deskto ptabs

          $(tabsEl).remove();
        } //bind click event


        $("#dropdownMenuVctabsMenu a").on("click", function () {
          var tabId = $(this).attr("data-vc-target");
          var tabTitle = $(this).text(); //hide all tab panes

          $(".vc-tab-pane").removeClass("active").removeClass("show"); //show clicked tab pane

          $(tabId).addClass("active").addClass("show"); //change dropdown text

          $("#dropdownMenuVctabs").html(tabTitle);
        });
      }
    }
  }
})(jQuery);

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blocks/_wp-block-vc-blocks-vctabs */ "./src/js/blocks/_wp-block-vc-blocks-vctabs.js");
/* harmony import */ var _blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blocks/_wp-block-cover */ "./src/js/blocks/_wp-block-cover.js");
/* harmony import */ var _blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1__);



(function ($) {
  $(document).scroll(function () {
    var $nav = $(".navbar-wrap");
    $nav.toggleClass('scrolled', $(this).scrollTop() > 10);
  });
  $(document).ready(function () {
    var announcementBar = getCookie('announcementBar');

    if (!announcementBar) {
      $(".announcement-bar-wrap").show();
    }

    $(".announcement-bar-wrap .fa-times").click(function () {
      setCookie('announcementBar', '1', '7');
    });

    function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
      var expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }

      return "";
    }
  });
})(jQuery);

/***/ }),

/***/ "./src/script.js":
/*!***********************!*\
  !*** ./src/script.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _scss_index_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scss/index.scss */ "./src/scss/index.scss");
/* harmony import */ var _scss_index_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scss_index_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/index */ "./src/js/index.js");



/***/ }),

/***/ "./src/scss/index.scss":
/*!*****************************!*\
  !*** ./src/scss/index.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=script.js.map
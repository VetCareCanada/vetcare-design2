<?php
wp_nav_menu(array(
    'theme_location' => 'primary',
    'container' => true,
    'menu_class' => '',
    'fallback_cb' => '__return_false',
    'items_wrap' => '<ul id="%1$s" class="navbar-nav me-auto mb-2 mb-md-0 %2$s">%3$s</ul>',
    'depth' => 2,
    'walker' => new bootstrap_5_wp_nav_menu_walker()
));
?>
    <div class="row phone-and-social">
        <div class="col d-xl-none"><a href="tel:<?php echo do_shortcode("[phone]") ?>"><i
                        class="fas fa-phone"></i> <?php echo do_shortcode("[phone]") ?></a></div>

    </div>
<?php if (get_field("main_nav_cta_button_url", "option")) :
    $bigButtonLink = get_field("main_nav_cta_button_url", "option");
    ?>
    <div class="big-button">
        <a class="btn btn-primary" href="<?php echo $bigButtonLink['url']; ?>"
           target="<?php echo $bigButtonLink['target']; ?>">
            <?php echo $bigButtonLink['title']; ?>
        </a>
    </div>
<?php endif; ?>
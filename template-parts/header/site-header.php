<?php if (is_active_sidebar('utility-bar-1') || is_active_sidebar('utility-bar-2')) : ?>
    <div class="utility-bar">
        <?php if (is_active_sidebar('utility-bar-1')) : ?>
            <div class="left">
                <?php dynamic_sidebar('utility-bar-1'); ?>
            </div>
        <?php endif; ?>
        <?php if (is_active_sidebar('utility-bar-2')) : ?>
            <div class="right">
                <?php dynamic_sidebar('utility-bar-2'); ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
<header id="masthead" role="banner">
    <div class="navbar-wrap fixed-top">
        <nav class="navbar navbar-expand-xl navbar-dark  main-navbar">
            <?php get_template_part('template-parts/header/site-branding'); ?>
            <div class="offcanvas offcanvas-end" tabindex="-1" id="main-menu" aria-labelledby="offcanvasNavbarLabel">
                <div class="offcanvas-header">
                    <?php if (has_custom_logo()) : ?>
                        <div class="site-logo">
                            <img src="<?php echo esc_url(wp_get_attachment_url(get_theme_mod('custom_logo'))); ?>"/>
                        </div>
                    <?php endif; ?>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                            aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <?php get_template_part('template-parts/header/site-nav'); ?>
                </div>
            </div>
        </nav>
    </div>

    <!-- <nav class="navbar navbar-expand-xl navbar-light fixed-top main-navbar">
        <div class="container-fluid">





            </div>


    </nav>-->
</header><!-- #masthead -->


<a class="navbar-brand" href="<?php echo get_home_url(); ?>">
    <?php //if (has_custom_logo()) : ?>
        <div class="site-logo">
            <img  class="main-logo" src="<?php echo esc_url(wp_get_attachment_url(get_theme_mod('custom_logo'))); ?>" />
            <?php if (get_field("site_white_logo", "option")) :
                $siteWhiteLogo = get_field("site_white_logo", "option");
                ?>
                <img class="white-logo" title="<?php echo $siteWhiteLogo['title']; ?>"
                     alt="<?php echo $siteWhiteLogo['alt']; ?>" src="<?php echo $siteWhiteLogo['url']; ?>"/>
            <?php endif; ?>
        </div>
    <?php //endif; ?>
</a>
<button class="navbar-toggler" type="button" data-bs-toggle="offcanvas"  data-bs-target="#main-menu"
        aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
